import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SettingProfilPageRoutingModule } from './setting-profil-routing.module';

import { SettingProfilPage } from './setting-profil.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SettingProfilPageRoutingModule
  ],
  declarations: [SettingProfilPage]
})
export class SettingProfilPageModule {}

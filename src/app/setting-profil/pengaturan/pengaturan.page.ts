import { Component} from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-pengaturan',
  templateUrl: './pengaturan.page.html',
  styleUrls: ['./pengaturan.page.scss'],
})
export class PengaturanPage  {

  constructor(
    private alertCtrl: AlertController
  )
   {} 

   async showAlert() {
     await this.alertCtrl.create({
       subHeader: "Masukkan Data",
       inputs: [
         { type: 'text', name: 'sandi', placeholder: "Kata Sandi Baru"},
         { type: 'text', name: 'sandi', placeholder: "Ulangi Kata Sandi Baru"}
       ],
          buttons: [
          {
            text: "Apply", handler: (res) => {
              console.log(res.sandi);
            }
          },
          {
            text: "Cancel"
          }
        ]
       
     }).then(res => res.present())
   }

   async showAlert2() {
    await this.alertCtrl.create({
      subHeader: "Masukkan Data",
      inputs: [
        { type: 'text', name: 'nama', placeholder: "Nama Pengguna Baru"},
        { type: 'text', name: 'nama', placeholder: "Ulangi Nama Pengguna Baru"},

      ],
         buttons: [
         {
           text: "Apply", handler: (res) => {
             console.log(res.nama);
           }
         },
         {
           text: "Cancel"
         }
       ]
      
    }).then(res => res.present())
  }
}

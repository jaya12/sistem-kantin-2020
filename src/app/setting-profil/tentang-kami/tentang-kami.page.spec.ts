import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TentangKamiPage } from './tentang-kami.page';

describe('TentangKamiPage', () => {
  let component: TentangKamiPage;
  let fixture: ComponentFixture<TentangKamiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TentangKamiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TentangKamiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettingProfilPage } from './setting-profil.page';

const routes: Routes = [
  {
    path: '',
    component: SettingProfilPage
  },
  {
    path: 'tentang-kami',
    loadChildren: () => import('./tentang-kami/tentang-kami.module').then( m => m.TentangKamiPageModule)
  },
  {
    path: 'pengaturan',
    loadChildren: () => import('./pengaturan/pengaturan.module').then( m => m.PengaturanPageModule)
  },
  {
    path: 'hubungi-kami',
    loadChildren: () => import('./hubungi-kami/hubungi-kami.module').then( m => m.HubungiKamiPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SettingProfilPageRoutingModule {}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SettingProfilPage } from './setting-profil.page';

describe('SettingProfilPage', () => {
  let component: SettingProfilPage;
  let fixture: ComponentFixture<SettingProfilPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingProfilPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SettingProfilPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

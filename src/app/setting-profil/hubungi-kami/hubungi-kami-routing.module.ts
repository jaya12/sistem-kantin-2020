import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HubungiKamiPage } from './hubungi-kami.page';

const routes: Routes = [
  {
    path: '',
    component: HubungiKamiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HubungiKamiPageRoutingModule {}

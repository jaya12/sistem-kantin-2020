import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-setting-profil',
  templateUrl: './setting-profil.page.html',
  styleUrls: ['./setting-profil.page.scss'],
})
export class SettingProfilPage implements OnInit {

  constructor(
    public alertController: AlertController,
    public router:Router,
    public modalController: ModalController,
    public db: AngularFirestore,
    public auth: AngularFireAuth
  ) { }

  authData:any;
  ngOnInit() {
    this.auth.auth.onAuthStateChanged(res=>{
      this.authData = res;
      this.getUser();
    })
  }

  userData:any;
  getUser()
  {
    this.db.collection('users').doc(this.authData.uid).valueChanges().subscribe(res=>{
      this.userData = res;
    })
  }
  

  async logout() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Keluar Aplikasi?',
      message: 'Untuk mengakses aplikasi ini, Anda perlu login kembali setelah keluar aplikasi.',
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            return false;
          }
        }, {
          text: 'Ya, Keluar',
          handler: () => {
            this.auth.auth.signOut().then(res => {
              this.router.navigate(['/home']);
            });
          }
        }
      ]
    });

    await alert.present();
  }
}

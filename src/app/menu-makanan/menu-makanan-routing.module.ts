import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuMakananPage } from './menu-makanan.page';

const routes: Routes = [
  {
    path: '',
    component: MenuMakananPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuMakananPageRoutingModule {}

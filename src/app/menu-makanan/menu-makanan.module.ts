import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenuMakananPageRoutingModule } from './menu-makanan-routing.module';

import { MenuMakananPage } from './menu-makanan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuMakananPageRoutingModule
  ],
  declarations: [MenuMakananPage]
})
export class MenuMakananPageModule {}

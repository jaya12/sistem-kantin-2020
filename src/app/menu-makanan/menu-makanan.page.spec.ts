import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MenuMakananPage } from './menu-makanan.page';

describe('MenuMakananPage', () => {
  let component: MenuMakananPage;
  let fixture: ComponentFixture<MenuMakananPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuMakananPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MenuMakananPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

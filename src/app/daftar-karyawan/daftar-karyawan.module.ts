import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DaftarKaryawanPageRoutingModule } from './daftar-karyawan-routing.module';

import { DaftarKaryawanPage } from './daftar-karyawan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DaftarKaryawanPageRoutingModule
  ],
  declarations: [DaftarKaryawanPage]
})
export class DaftarKaryawanPageModule {}

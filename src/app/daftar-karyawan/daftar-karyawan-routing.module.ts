import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DaftarKaryawanPage } from './daftar-karyawan.page';

const routes: Routes = [
  {
    path: '',
    component: DaftarKaryawanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DaftarKaryawanPageRoutingModule {}

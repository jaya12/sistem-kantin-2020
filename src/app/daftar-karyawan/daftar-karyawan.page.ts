import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from "@angular/fire/firestore";

@Component({
  selector: 'app-daftar-karyawan',
  templateUrl: './daftar-karyawan.page.html',
  styleUrls: ['./daftar-karyawan.page.scss'],
})
export class DaftarKaryawanPage implements OnInit {

  constructor(
   public database : AngularFirestore
  ) { }

  ngOnInit() {
    this.get_user();
  }

users : any;

get_user(){
  this.database.collection('users').valueChanges().subscribe(data => {
    this.users = data;

  })
}

}

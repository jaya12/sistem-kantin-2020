import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DaftarKaryawanPage } from './daftar-karyawan.page';

describe('DaftarKaryawanPage', () => {
  let component: DaftarKaryawanPage;
  let fixture: ComponentFixture<DaftarKaryawanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DaftarKaryawanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DaftarKaryawanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

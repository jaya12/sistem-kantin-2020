import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from "@angular/fire/firestore";


@Component({
  selector: 'app-pembayaran-transaksi-langsung',
  templateUrl: './pembayaran-transaksi-langsung.page.html',
  styleUrls: ['./pembayaran-transaksi-langsung.page.scss'],
})
export class PembayaranTransaksiLangsungPage implements OnInit {

  constructor(
    private database : AngularFirestore,

  ) { }

  ngOnInit() {
    this.get_detail()
  }
  data_pesanan : any;
  get_detail(){
   
    this.database.collection('data_pesanan').valueChanges({idField: 'id'}).subscribe(data => {
        this.data_pesanan = data;
        this.data_pesanan = this.data_pesanan.map(res => {
          res['jumlah'] = 0;
          return res;
        });
      })
    
  

}
}
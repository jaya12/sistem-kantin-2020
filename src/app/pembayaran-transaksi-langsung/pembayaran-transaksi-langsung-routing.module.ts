import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PembayaranTransaksiLangsungPage } from './pembayaran-transaksi-langsung.page';

const routes: Routes = [
  {
    path: '',
    component: PembayaranTransaksiLangsungPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PembayaranTransaksiLangsungPageRoutingModule {}

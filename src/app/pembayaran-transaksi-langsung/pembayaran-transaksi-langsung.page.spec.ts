import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PembayaranTransaksiLangsungPage } from './pembayaran-transaksi-langsung.page';

describe('PembayaranTransaksiLangsungPage', () => {
  let component: PembayaranTransaksiLangsungPage;
  let fixture: ComponentFixture<PembayaranTransaksiLangsungPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PembayaranTransaksiLangsungPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PembayaranTransaksiLangsungPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

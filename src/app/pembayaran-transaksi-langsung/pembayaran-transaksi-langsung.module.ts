import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PembayaranTransaksiLangsungPageRoutingModule } from './pembayaran-transaksi-langsung-routing.module';

import { PembayaranTransaksiLangsungPage } from './pembayaran-transaksi-langsung.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PembayaranTransaksiLangsungPageRoutingModule
  ],
  declarations: [PembayaranTransaksiLangsungPage]
})
export class PembayaranTransaksiLangsungPageModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransaksiLangsung2Page } from './transaksi-langsung2.page';

const routes: Routes = [
  {
    path: '',
    component: TransaksiLangsung2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransaksiLangsung2PageRoutingModule {}

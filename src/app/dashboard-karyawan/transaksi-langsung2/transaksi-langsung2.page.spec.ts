import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TransaksiLangsung2Page } from './transaksi-langsung2.page';

describe('TransaksiLangsung2Page', () => {
  let component: TransaksiLangsung2Page;
  let fixture: ComponentFixture<TransaksiLangsung2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransaksiLangsung2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TransaksiLangsung2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

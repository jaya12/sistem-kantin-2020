import { Component, OnInit } from '@angular/core';
import {AngularFirestore  } from "@angular/fire/firestore";
import { Router } from "@angular/router";

@Component({
  selector: 'app-transaksi-langsung2',
  templateUrl: './transaksi-langsung2.page.html',
  styleUrls: ['./transaksi-langsung2.page.scss'],
})
export class TransaksiLangsung2Page implements OnInit {

  constructor(
    public database : AngularFirestore,
    private router:Router
  ) { }

  ngOnInit() {
    this.get_data();
    this.get_pesanan();
  }

data_makan : any;
data_pesanan : any;
users : any;

get_data(){
  this.database.collection('data_makan').valueChanges({idField: 'id'}).subscribe(data => {
    this.data_makan = data;
    this.data_makan = this.data_makan.map(res => {
      res['jumlah'] = 0;
      return res;
    });
  })
}

dataPesanan:any = [];
tambah(index: number) {
  this.data_makan[index].jumlah += 1;
  this.parseData(index);
}

parseData(index) {
  this.dataPesanan = []
  for(var i=0; i<this.data_makan.length; i++) {
    if(this.data_makan[i].jumlah != 0) {
      var idx = this.dataPesanan.map((e) => {return e == undefined ? 0:e.id}).indexOf(this.data_makan[i].id);
      if(idx == -1) {
        this.dataPesanan.push(this.data_makan[i]);
      } else {
        this.dataPesanan[index] = this.data_makan[i];
      }
    }
  }
}

hapus(index: number) {
  this.data_makan[index].jumlah -= 1;
}

pesan() {
  console.log(this.dataPesanan);
  var data = {
    nama : "apip",
    produk: this.dataPesanan
  }
  var doc = new Date().getTime().toString() + '' + [Math.floor((Math.random() * 10000))];
  this.database.collection('data_pesanan').doc(doc).set(data).then(res => {
    this.router.navigate(["/pembayaran-transaksi-langsung"])
    this.dataPesanan = [];
    this.get_data();
  })
}
get_pesanan(){
  this.database.collection('data_pesanan').valueChanges('nama').subscribe(data => {
    this.data_pesanan = data;
  })
}

}

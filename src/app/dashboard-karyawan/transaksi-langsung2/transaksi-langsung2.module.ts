import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TransaksiLangsung2PageRoutingModule } from './transaksi-langsung2-routing.module';

import { TransaksiLangsung2Page } from './transaksi-langsung2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TransaksiLangsung2PageRoutingModule
  ],
  declarations: [TransaksiLangsung2Page]
})
export class TransaksiLangsung2PageModule {}

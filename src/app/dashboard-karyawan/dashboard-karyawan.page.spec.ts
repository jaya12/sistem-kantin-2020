import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DashboardKaryawanPage } from './dashboard-karyawan.page';

describe('DashboardKaryawanPage', () => {
  let component: DashboardKaryawanPage;
  let fixture: ComponentFixture<DashboardKaryawanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardKaryawanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DashboardKaryawanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardKaryawanPage } from './dashboard-karyawan.page';

const routes: Routes = [
  {
    path: '',
    component: DashboardKaryawanPage
  },  {
    path: 'transaksi-langsung2',
    loadChildren: () => import('./transaksi-langsung2/transaksi-langsung2.module').then( m => m.TransaksiLangsung2PageModule)
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardKaryawanPageRoutingModule {}

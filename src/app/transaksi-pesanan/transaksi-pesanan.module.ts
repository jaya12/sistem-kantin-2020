import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TransaksiPesananPageRoutingModule } from './transaksi-pesanan-routing.module';

import { TransaksiPesananPage } from './transaksi-pesanan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TransaksiPesananPageRoutingModule
  ],
  declarations: [TransaksiPesananPage]
})
export class TransaksiPesananPageModule {}

import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { datapem } from "../models/datapemesan.models";
import { ToastController, LoadingController } from '@ionic/angular';
import { Router } from "@angular/router";
@Component({
  selector: 'app-transaksi-pesanan',
  templateUrl: './transaksi-pesanan.page.html',
  styleUrls: ['./transaksi-pesanan.page.scss'],
})
export class TransaksiPesananPage implements OnInit {
data = {}  as datapem
  constructor(
    public database : AngularFirestore,
    private toastr: ToastController,
    private loadingCtrl : LoadingController,
    private router : Router
  ) { }

  ngOnInit() {
    this.get_data();
  }
  data_makan : any;
  data_pesanan : any;
  users : any;
  get_data(){
    this.database.collection('data_makan').valueChanges({idField: 'id'}).subscribe(data => {
      this.data_makan = data;
      this.data_makan = this.data_makan.map(res => {
        res['jumlah'] = 0;
        return res;
      });
    })
  }
  dataPesanan:any = [];
tambah(index: number) {
  this.data_makan[index].jumlah += 1;
  this.parseData(index);
}

parseData(index) {
  this.dataPesanan = []
  for(var i=0; i<this.data_makan.length; i++) {
    if(this.data_makan[i].jumlah != 0) {
      var idx = this.dataPesanan.map((e) => {return e == undefined ? 0:e.id}).indexOf(this.data_makan[i].id);
      if(idx == -1) {
        this.dataPesanan.push(this.data_makan[i]);
      } else {
        this.dataPesanan[index] = this.data_makan[i];
      }
    }
  }
}
hapus(index: number) {
  this.data_makan[index].jumlah -= 1;
}



showToast(message : string, danger){
  this.toastr.create({
    message : message,
    duration : 3000,
    position : 'top',
    color : danger

  })
  .then(toasData => toasData.present())
}

formValdation(){
  if(!this.data.pemesan){
    this.showToast("masukan nama","danger")
    return false
  }
  if(!this.data.no){
    this.showToast("masukan no","danger")
    return false
  }
  if(!this.data.alamat){
    this.showToast("masukan alamat","danger")
    return false
  }if(!this.data.jam){
    this.showToast("masukan jam","danger")
    return false
  }if(!this.data.tanggal){
    this.showToast("masukan tanggal","danger")
    return false
  }
}
async pesan( data : datapem, success){
  if(this.formValdation){
    const load = await this.loadingCtrl.create({
      message : "please wait..",
      duration : 3000
    })
  }
  try{
    
    var getall = {
      data1: this.dataPesanan,
      data2 : this.data
      
    }
    var doc = new Date().getTime().toString() + '' + [Math.floor((Math.random() * 10000))];
    this.database.collection('transaksi_pbesar').doc(doc).set(getall).then(res => {
      this.router.navigate(["/pembayaran-transaksi-langsung"])
      this.dataPesanan = [];
      this.get_data();
    })
  }catch(err){
    this.showToast(err,'danger');
  }
}
}

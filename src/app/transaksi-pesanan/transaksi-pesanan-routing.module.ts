import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransaksiPesananPage } from './transaksi-pesanan.page';

const routes: Routes = [
  {
    path: '',
    component: TransaksiPesananPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransaksiPesananPageRoutingModule {}

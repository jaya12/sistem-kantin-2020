import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TransaksiPesananPage } from './transaksi-pesanan.page';

describe('TransaksiPesananPage', () => {
  let component: TransaksiPesananPage;
  let fixture: ComponentFixture<TransaksiPesananPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransaksiPesananPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TransaksiPesananPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

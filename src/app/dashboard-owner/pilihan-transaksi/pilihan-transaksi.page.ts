import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-pilihan-transaksi',
  templateUrl: './pilihan-transaksi.page.html',
  styleUrls: ['./pilihan-transaksi.page.scss'],
})
export class PilihanTransaksiPage implements OnInit {

  constructor(
    public modalCtrl: ModalController,
    public router: Router
  ) { }

  ngOnInit() {
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  langsung() {
    this.router.navigate(["./transaksi-langsung"])
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  pesanan() {
    this.router.navigate(["./transaksi-pesanan"])
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }
}

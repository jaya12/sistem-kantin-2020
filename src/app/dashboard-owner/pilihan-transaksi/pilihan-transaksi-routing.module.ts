import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PilihanTransaksiPage } from './pilihan-transaksi.page';

const routes: Routes = [
  {
    path: '',
    component: PilihanTransaksiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PilihanTransaksiPageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PilihanTransaksiPageRoutingModule } from './pilihan-transaksi-routing.module';

import { PilihanTransaksiPage } from './pilihan-transaksi.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PilihanTransaksiPageRoutingModule
  ],
  declarations: [PilihanTransaksiPage]
})
export class PilihanTransaksiPageModule {}

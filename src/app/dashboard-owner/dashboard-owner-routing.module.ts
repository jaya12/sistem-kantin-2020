import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardOwnerPage } from './dashboard-owner.page';

const routes: Routes = [
  {
    path: '',
    component: DashboardOwnerPage
  },
  {
    path: 'pilihan-transaksi',
    loadChildren: () => import('./pilihan-transaksi/pilihan-transaksi.module').then( m => m.PilihanTransaksiPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardOwnerPageRoutingModule {}

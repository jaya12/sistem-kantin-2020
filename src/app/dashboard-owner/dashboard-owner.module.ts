import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardOwnerPageRoutingModule } from './dashboard-owner-routing.module';

import { DashboardOwnerPage } from './dashboard-owner.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashboardOwnerPageRoutingModule
  ],
  declarations: [DashboardOwnerPage]
})
export class DashboardOwnerPageModule {}

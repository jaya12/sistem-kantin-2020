import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DashboardOwnerPage } from './dashboard-owner.page';

describe('DashboardOwnerPage', () => {
  let component: DashboardOwnerPage;
  let fixture: ComponentFixture<DashboardOwnerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardOwnerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DashboardOwnerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PilihanTransaksiPage } from './pilihan-transaksi/pilihan-transaksi.page';

@Component({
  selector: 'app-dashboard-owner',
  templateUrl: './dashboard-owner.page.html',
  styleUrls: ['./dashboard-owner.page.scss'],
})
export class DashboardOwnerPage implements OnInit {

  constructor(public modalController: ModalController) { }

  ngOnInit() {
  }

  async PilihanTransaksi() {
    const modal = await this.modalController.create({
      component: PilihanTransaksiPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}

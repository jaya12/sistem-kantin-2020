import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { Router } from "@angular/router";
import { AngularFirestore } from "@angular/fire/firestore";
import { ToastController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(
    private auth:AngularFireAuth, 
    private router:Router,
    private db : AngularFirestore,
    private toastr: ToastController,
    public loadingController: LoadingController
  ) { }

  ngOnInit() {
  }

  email : any;
  password : any;
  login(){
    this.loading();
    this.auth.auth.signInWithEmailAndPassword(this.email,this.password).then(res => {
      this.cek_akses(res)
    }).catch (error => {
      this.toast('Email atau password anda salah!', 'danger');
    })
  }

  async loading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'ditunggu ya',
      duration: 2000
    });
    await loading.present();
  }

  async toast(message, danger)
    {
        const toast = await this.toastr.create({
            message:message,
            position: 'top',
            color: danger,
            duration:2000
        });
        toast.present();
     }

  data_user : any;
  cek_akses(res){
    console.log(res)
    var id_user = res.user.uid;
    this.db.collection("users").doc(id_user).valueChanges().subscribe(data => {
      console.log(data)
     this.data_user = data;
      if(this.data_user.role == "owner"){
        this.router.navigate(["/dashboard-owner"])
      }else if(this.data_user.role == "karyawan"){
        this.router.navigate(["/dashboard-karyawan"])
      }else{
        alert("gagal")
      }
    })
  
  }
}

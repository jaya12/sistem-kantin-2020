import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TransaksiLangsungPageRoutingModule } from './transaksi-langsung-routing.module';

import { TransaksiLangsungPage } from './transaksi-langsung.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TransaksiLangsungPageRoutingModule
  ],
  declarations: [TransaksiLangsungPage]
})
export class TransaksiLangsungPageModule {}

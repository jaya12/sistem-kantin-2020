import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransaksiLangsungPage } from './transaksi-langsung.page';

const routes: Routes = [
  {
    path: '',
    component: TransaksiLangsungPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransaksiLangsungPageRoutingModule {}

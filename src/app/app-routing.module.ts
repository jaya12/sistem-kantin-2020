import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'dashboard-owner',
    loadChildren: () => import('./dashboard-owner/dashboard-owner.module').then( m => m.DashboardOwnerPageModule)
  },
  {
    path: 'transaksi-langsung',
    loadChildren: () => import('./transaksi-langsung/transaksi-langsung.module').then( m => m.TransaksiLangsungPageModule)
  },
  {
    path: 'menu-makanan',
    loadChildren: () => import('./menu-makanan/menu-makanan.module').then( m => m.MenuMakananPageModule)
  },

  {
  path: 'setting-profil',
  loadChildren: () => import('./setting-profil/setting-profil.module').then( m => m.SettingProfilPageModule)
  },
  {
    path: 'daftar-karyawan',
    loadChildren: () => import('./daftar-karyawan/daftar-karyawan.module').then( m => m.DaftarKaryawanPageModule)
  },
  {
    path: 'pembayaran-transaksi-langsung',
    loadChildren: () => import('./pembayaran-transaksi-langsung/pembayaran-transaksi-langsung.module').then( m => m.PembayaranTransaksiLangsungPageModule)
  },
  {
    path: 'dashboard-karyawan',
    loadChildren: () => import('./dashboard-karyawan/dashboard-karyawan.module').then( m => m.DashboardKaryawanPageModule)
  },  {
    path: 'perekapan-barang',
    loadChildren: () => import('./perekapan-barang/perekapan-barang.module').then( m => m.PerekapanBarangPageModule)
  },
  {
    path: 'detail-pesanan',
    loadChildren: () => import('./detail-pesanan/detail-pesanan.module').then( m => m.DetailPesananPageModule)
  },
  {
    path: 'input-barang',
    loadChildren: () => import('./input-barang/input-barang.module').then( m => m.InputBarangPageModule)
  },
  {
    path: 'transaksi-pesanan',
    loadChildren: () => import('./transaksi-pesanan/transaksi-pesanan.module').then( m => m.TransaksiPesananPageModule)
  },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

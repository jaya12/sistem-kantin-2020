import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PerekapanBarangPageRoutingModule } from './perekapan-barang-routing.module';

import { PerekapanBarangPage } from './perekapan-barang.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PerekapanBarangPageRoutingModule
  ],
  declarations: [PerekapanBarangPage]
})
export class PerekapanBarangPageModule {}

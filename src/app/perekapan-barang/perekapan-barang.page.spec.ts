import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PerekapanBarangPage } from './perekapan-barang.page';

describe('PerekapanBarangPage', () => {
  let component: PerekapanBarangPage;
  let fixture: ComponentFixture<PerekapanBarangPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerekapanBarangPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PerekapanBarangPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

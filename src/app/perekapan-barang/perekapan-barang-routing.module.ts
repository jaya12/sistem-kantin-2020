import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PerekapanBarangPage } from './perekapan-barang.page';

const routes: Routes = [
  {
    path: '',
    component: PerekapanBarangPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PerekapanBarangPageRoutingModule {}

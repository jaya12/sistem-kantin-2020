import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InputBarangPage } from './input-barang.page';

describe('InputBarangPage', () => {
  let component: InputBarangPage;
  let fixture: ComponentFixture<InputBarangPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputBarangPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InputBarangPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

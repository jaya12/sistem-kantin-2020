import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InputBarangPageRoutingModule } from './input-barang-routing.module';

import { InputBarangPage } from './input-barang.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InputBarangPageRoutingModule
  ],
  declarations: [InputBarangPage]
})
export class InputBarangPageModule {}

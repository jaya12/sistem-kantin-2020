import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InputBarangPage } from './input-barang.page';

const routes: Routes = [
  {
    path: '',
    component: InputBarangPage
  },
  {
    path: 'stok-barang',
    loadChildren: () => import('./stok-barang/stok-barang.module').then( m => m.StokBarangPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InputBarangPageRoutingModule {}

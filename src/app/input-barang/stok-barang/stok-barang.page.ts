import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from "@angular/fire/firestore";
import { Router } from "@angular/router";
import { ToastController, LoadingController } from '@ionic/angular';
import { Post } from "../../models/user.models";
@Component({
  selector: 'app-stok-barang',
  templateUrl: './stok-barang.page.html',
  styleUrls: ['./stok-barang.page.scss'],
})
export class StokBarangPage implements OnInit {
data_makan : any;
post  = {} as Post;
  constructor(
    private db : AngularFirestore,
    private router : Router,
    private toast : ToastController,
    private loadCtrl : LoadingController
  ) { }

  ngOnInit() {
   
    this.getPost()
  }


 async getPost(){
   
  let load = await this.loadCtrl.create({
    message : "mohon tunggu",
    duration : 3000
  });
  await load.present();
  try{
    this.db.collection("data_makan")
    .snapshotChanges()
    .subscribe(data => {
      this.data_makan = data.map(e => {
        return{
          id : e.payload.doc.id,
          nama : e.payload.doc.data()['nama'],
          harga : e.payload.doc.data()['harga'],
          stok : e.payload.doc.data()['stok'],
          keterangan : e.payload.doc.data()['keterangan'],
        };
      });
    });
    await load.dismiss();
  }catch(e){
    
    this.showToast(e);
    }
 }

 showToast(message : string){
   this.toast.create({
    message : message,
    duration : 3000
   })
 }
 async delete(id : 1){
  let loader = this.loadCtrl.create({
    message : 'please wait',

  });
  (await loader).present()
   await this.db.doc("post/" + id).delete();
   (await loader).dismiss()
}
}


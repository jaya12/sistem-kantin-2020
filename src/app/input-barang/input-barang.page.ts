import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from "@angular/fire/firestore";
import { Router } from "@angular/router";
import { ToastController, LoadingController } from '@ionic/angular';
import { Post } from "../models/user.models";
@Component({
  selector: 'app-input-barang',
  templateUrl: './input-barang.page.html',
  styleUrls: ['./input-barang.page.scss'],
})
export class InputBarangPage implements OnInit {
  post  = {} as Post;
  constructor(
    private db : AngularFirestore,
    private router : Router,
    private toastr: ToastController,
    private loadingCtrl : LoadingController
  ) { }

 
  ngOnInit() {
  }

  async add(post : Post, success){
    if(this.formValidation())
    {
      const load = await this.loadingCtrl.create({
       message: "please waitt...." ,
       duration : 3000

      });
      await load.present();

      try{
        
        var doc = new Date().getTime().toString() + '' + [Math.floor((Math.random() * 10000))];
   
        this.db.collection('data_makan').doc(doc).set(this.post).then(user =>{
        this.router.navigate(["/input-barang/stok-barang"])
    })
      }catch (e){
        this.showToast(e,'danger');
      }
      (await load).dismiss();
    }
  }

  formValidation(){
    if(!this.post.nama){
      this.showToast("masuka nama produk",'danger');
      return false;  
    } 
    
    if(!this.post.harga){
      this.showToast("masukan harga",'danger')
      return false;
    } 
   
    if(!this.post.keterangan){
      this.showToast("masukan deskripsi",'danger')
      return false
    }
    if(!this.post.stok){
      this.showToast("masukan jumlah stok",'danger')
      return false
    }
  
    
    return true;  
  }

  showToast(message: string, danger){
    this.toastr
    .create({
      message : message,
      duration: 3000,
      position : 'top',
      color : danger
      
    })
    .then(toastData => toastData.present());
  }

  

}

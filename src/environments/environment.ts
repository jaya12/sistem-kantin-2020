// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyB2f2s0nnMwFvfmZ8lviF4yyy9VcSGYlQQ",
    authDomain: "sistem-kantin-2020.firebaseapp.com",
    databaseURL: "https://sistem-kantin-2020-default-rtdb.firebaseio.com",
    projectId: "sistem-kantin-2020",
    storageBucket: "sistem-kantin-2020.appspot.com",
    messagingSenderId: "332209618181",
    appId: "1:332209618181:web:1c54a60a23ce48e95ecb6c",
    measurementId: "G-T0TTGKLB3W"
  }
}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
